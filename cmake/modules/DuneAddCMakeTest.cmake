#[=======================================================================[.rst:
DuneAddCMakeTest
----------------

Macros that provides tools for testing the CMake functions.


.. cmake:command:: dune_add_cmake_test

  .. code-block:: cmake

    dune_add_cmake_test(<name> <dir>)

  Create a new test with name ``<name>`` that runs CMake in the given
  directory ``<dir>``.

#]=======================================================================]
include_guard(GLOBAL)

set(build_generator_args --build-generator ${CMAKE_GENERATOR})
if(CMAKE_GENERATOR_PLATFORM)
  list(APPEND build_generator_args --build-generator-platform ${CMAKE_GENERATOR_PLATFORM})
endif()
if(CMAKE_GENERATOR_TOOLSET)
  list(APPEND build_generator_args --build-generator-toolset ${CMAKE_GENERATOR_TOOLSET})
endif()


macro(dune_add_cmake_test _name)
  cmake_parse_arguments(ARG "" "SOURCE_DIR" "MODULE_PATH" ${ARGN})

  set(build_options)
  if(ARG_MODULE_PATH)
    string(JOIN ":" _paths "${ARG_MODULE_PATH}")
    set(build_options "--build-options" "-DCMAKE_MODULE_PATH=${_paths}")
  endif()

  if(ARG_SOURCE_DIR)
    set(_dir ${ARG_SOURCE_DIR})
  else()
    set(_dir ${_name})
  endif()

  set(DUNE_REENABLE_ADD_TEST 1)
  add_test(NAME ${_name}
    COMMAND "${CMAKE_CTEST_COMMAND}"
      --build-and-test "${CMAKE_CURRENT_SOURCE_DIR}/${_dir}"
                       "${CMAKE_CURRENT_BINARY_DIR}/${_dir}"
      ${build_generator_args}
      ${build_options}
      --test-command "${CMAKE_CTEST_COMMAND}")
  set(DUNE_REENABLE_ADD_TEST 0)
endmacro(dune_add_cmake_test)
